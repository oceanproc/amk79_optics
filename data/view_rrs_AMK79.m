% ��������� ���� � ������� RRS

clear all
close all
clc

load('RRS_AMK79.mat')

disp('����������������� ������ �������:')
stn_double = RRS_AMK79{1}(:) % ����������������� ������ ������� � ������� double

% ���������� ������ ������� ��� ���������
num_to_plot = 1:length(stn_double);
% num_to_plot = [1 3 4];

% ����������������� ������ ������� ��� ���������
stn_to_plot = [6604 6607 6609];
stn_to_plot = [];
% *���� stn_to_plot ������, �� ����� �� ���������� ������� num_to_plot

if ~isempty(stn_to_plot)
    n = 0;
    num_to_plot = [];
    for i = 1:length(stn_to_plot)
        num = find(stn_to_plot(i) == stn_double);
        if ~isempty(num)
            n = n + 1;
            num_to_plot(n,1) = num;
        end
    end
end

figure, hold on
stn_plotted = [];
for i = 1:length(num_to_plot)
    
    num = num_to_plot(i);
    stn_plotted{i,1} = num2str(stn_double(num));
    
    w   = RRS_AMK79{num+1}(:,1); % ����� �����
    rrs = RRS_AMK79{num+1}(:,2); % rrs
    
    %
    plot(w,rrs)
    grid on
    grid minor
    xlabel('wavelength, nm')
    ylabel('rrs, sr^{-1}')
    
end

legend(stn_plotted)