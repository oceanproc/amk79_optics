% �������������� ��� ������ RRS ������� GIOP
clear all
close all
clc

%% ���������� ���������� � ������� � ��������� ��
mdir = fileparts(mfilename('fullpath'));
if isunix s = '/'; else s = '\'; end
pos  = find(mdir == s);
dpath_data = [mdir(1:pos(end)),'data',s];
fpath_data = [dpath_data,'RRS_AMK79.mat'];
[W,RRS,num_of_spectra,id_stn] = fun_AMK79_load_rrs_moscow(fpath_data);

%% ��������� �������������
w_lim = [400 650];

%% ��������� ����������� ������
[F,f,gof,kstr,k,k_0,k_lim,rrs_below] = fun_rrs_fit_GIOP_multi(W,RRS,[],[],[],w_lim,[]);



% to_plot = 1;
% fun_rrs_model_GIOP_global(w, k(1), k(2), k(3), k(4), k(5), k(6), to_plot);