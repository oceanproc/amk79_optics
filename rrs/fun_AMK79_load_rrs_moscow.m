function [w,rrs,num_of_spectra,id_stn] = fun_AMK79_load_rrs_moscow(fpath)
% ������� ��� ���������� ������ RRS, ���������� � ������� ����������
% ro-����� (�� ���, �. ������)
%
% � ����� �������������� ��� ����������� �������:
% - ��������� ro-���� �� ���
%

load(fpath);

stn_double = RRS_AMK79{1}(:); % ����������������� ������ ������� � ������� double
num_of_spectra = length(stn_double);

% ����������������� ������ ������� � ���� ������� ����� � ��������� �����
id_stn = cell(num_of_spectra,1);
for i = 1:length(id_stn)
    id_stn{i} = num2str(stn_double(i));
end

w   = cell(num_of_spectra,1);
rrs = w;
for i = 1:num_of_spectra
    w{i}   = RRS_AMK79{i+1}(:,1);
    rrs{i} = RRS_AMK79{i+1}(:,2);
end