% �������������� ������ RRS ������� GIOP
clear all
close all
clc

%% ���������� ���������� � ������� � ��������� ��
mdir = fileparts(mfilename('fullpath'));
if isunix s = '/'; else s = '\'; end
pos  = find(mdir == s);
dpath_data = [mdir(1:pos(end)),'data',s];
fpath_data = [dpath_data,'RRS_AMK79.mat'];
[W,RRS,num_of_spectra,id_stn] = fun_AMK79_load_rrs_moscow(fpath_data);

%% �������� ������ ��� �������
id_stn
num_stn_select = 13;
if isempty(num_stn_select)
    id_stn_select = '6566';
    idx = find(strcmp(id_stn,id_stn_select));
else
    idx = num_stn_select;
end
w   = W{idx};
rrs = RRS{idx};

%% ��������� �������������
w_lim = [400 650];

%% ��������� ����������� ������
[F,f,gof,kstr,k,k_0,k_lim,rrs_below] = fun_rrs_fit_GIOP(w,rrs,[],[],[],w_lim);

%% ���������� ����������
figure, hold on
plot(w,rrs_below,'k.')
plot(w,f(w),'g-','LineWidth',2)
ylim = get(gca,'ylim');
plot(w_lim(1)*[1 1],ylim,'b--')
plot(w_lim(2)*[1 1],ylim,'b--')
grid on
grid minor
xlabel('\lambda,nm')
ylabel('rrs below, sr^{-1}')
title(sprintf('stn = "%s"',id_stn{idx}))
legend('measured','modeled')
set(gca,'box','on')

to_plot = 1;
fun_rrs_model_GIOP_global(w, k(1), k(2), k(3), k(4), k(5), k(6), to_plot);