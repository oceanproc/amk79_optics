% �������������� ��� ������ RRS ������� GIOP
clear all
close all
clc

%% ���������� ���������� � ������� � ��������� ��
if isunix s = '/'; else s = '\'; end
% mdir = fileparts(mfilename('fullpath'));
% pos  = find(mdir == s);
% dpath_data = [mdir(1:pos(end)),'data',s];
% fpath_data = [dpath_data,'RRS_AMK79.mat'];
fpath_data = 'd:\NetStorage\Expeditions\2020.07_Sevastopol\#ASD\Level02\ASD_L2_2020.07_Sevastopol.mat';
% fpath_data = 'd:\NetStorage\Expeditions\2019.12_Keldysh79\#ASD\Level02\ASD_L2_2019.12_Keldysh79.mat';

fpath_srf = 'd:\Programs\bitbucket\hydrooptics\rrs\scanners_srf\data\srf_MODIS-AQUA.mat';
S = load(fpath_srf);

D = load(fpath_data);
% [W,RRS,num_of_spectra,id_stn];
W = D.W(:);
RRS = D.RRS_nir;
RRS = D.RRS_nir(:,1:8);
tp_local = D.t_utc;
tp_local = D.t_utc + 3/24;
rrs_above = RRS;

[sdir,sname] = fileparts(fpath_data);
sdir = [sdir,s,'GIOP',s];
sdir_img = [sdir,'img',s];
sname      = [sname,'_GIOP.mat'];

%% ��������� �������������
w_lim = [400 660];

rrs_mod = [];
for i = 1:size(RRS,2)

    w_mod    = [];
    rrs_mod_ = [];
    for j = 1:length(S.wave0)
        
        srf_ = S.band_fun{j}(W);
        w_int = round(S.wave_center(j));
        rrs_weight = sum(RRS(:,i).*srf_)/sum(srf_);
        w_mod = [w_mod;w_int];
        rrs_mod_ = [rrs_mod_;rrs_weight];
        
    end
    
    rrs_mod = [rrs_mod,rrs_mod_];
    
end

M = load('c:\temp\2mil_2020-07-02.mat');
M = load('c:\temp\2mil_2020-07-07.mat');
M = load('c:\temp\kats_2020-07-03.mat');

w_plot   = [412, 443, 469, 488, 531, 547, 555, 645, 667, 678];
% rrs_plot = [0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01];
% rrs_plot = rrs_plot./(0.52 + 1.7*rrs_plot);
rrs_plot = [M.Rrs_412, M.Rrs_443, M.Rrs_469, M.Rrs_488, M.Rrs_531, M.Rrs_547, M.Rrs_555, M.Rrs_645, M.Rrs_667, M.Rrs_678];
n = 5;
figure, hold on
plot(W,RRS(:,n),'b-')
plot(w_mod,rrs_mod(:,n),'ro')
plot(w_plot,rrs_plot,'ko-')
set(gca,'xlim',[400 800])
grid on
grid minor
xlabel('\lambda,nm')
ylabel('rrs above, sr^{-1}')
title(sprintf('%s',datestr(tp_local(n))))
legend('hyperspec ASD','multispec ASD','multispec MA')

%% ��������� ����������� ������
k_0_in   = [nan nan nan nan 0.019 nan];
% k_0_in   = [nan nan nan nan nan nan];
k_fix    = [  0   0   0   0     1   0];
[F,f,gof,kstr,k,k_0,k_lim,rrs_below] = fun_rrs_fit_GIOP_multi(W,RRS,k_0_in,[],k_fix,w_lim,[]);
[F_mod,f_mod,gof_mod,kstr_mod,k_mod,k_0_mod,k_lim_mod,rrs_below_mod] = fun_rrs_fit_GIOP_multi(w_mod,rrs_mod,k_0_in,[],k_fix,w_lim,[]);

%% ���������� ����������




for i = 1:size(rrs_below,2)

    sname_img1 = ['spectrum_',datestr(tp_local(i),'yyyymmddTHHMM'),'_',sname,'_GIOP.jpg'];
    sname_img2 = ['IOP_',datestr(tp_local(i),'yyyymmddTHHMM'),'_',sname,'_GIOP.jpg'];
    if ~isdir(sdir_img), mkdir(sdir_img); end
    
    w = W;
    f_ = f{i};
    k_ = k(:,i);
    gof_ = gof{i};
    rrs_ = rrs_below(:,i);

    f_mod_ = f_mod{i};
    k_mod_ = k_mod(:,i);
    gof_mod_ = gof_mod{i};
    rrs_mod_ = rrs_below_mod(:,i);
    
    close all
    
    figure, hold on
    plot(w,rrs_,'k.')
    plot(w,f_(w),'g-','LineWidth',1)
    plot(w,f_mod_(w),'r--','LineWidth',1)
    ylim = get(gca,'ylim');
    
    
    xlim     = [w_lim(1)-5, w_lim(2)+5];
    ylim_new = [0,1.05*max(rrs_(w >= w_lim(1) & w <= w_lim(2)))];
    
    for j = 1:length(S.wave0)
        
        w_vec = (w_lim(1):.05:w_lim(2))';
        srf = ylim_new(2)*S.band_fun{j}(w_vec);
        srf_ = S.band_fun{j}(w);
        
        w_int = round(S.wave_center(j));
        rrs_weight = sum(rrs_.*srf_)/sum(srf_);
        
        plot(w_vec,srf,'k-','color',2/3*[1 1 1])
        plot(w_int,rrs_weight,'ro')
        
    end
    plot(w_lim(1)*[1 1],ylim,'b--')
    plot(w_lim(2)*[1 1],ylim,'b--')
    
    set(gca,'xlim',xlim)
    set(gca,'ylim',ylim_new)
    
    grid on
    grid minor
    xlabel('\lambda,nm')
    ylabel('rrs below, sr^{-1}')
    title(sprintf('%s, R^2 = %.2f%%',datestr(tp_local(i)),100*gof_.rsquare))
    legend('measured','modeled','modeled srf','srf MODIS-A')
    set(gca,'box','on')
    
    print('-r250','-djpeg',[sdir_img,sname_img1])
    
    to_plot = 1;
    fun_rrs_model_GIOP_global(w, k_(1), k_(2), k_(3), k_(4), k_(5), k_(6), to_plot,w_lim);
    title(sprintf('modeled rrs, %s',datestr(tp_local(i))))
    print('-r250','-djpeg',[sdir_img,sname_img2])

   
    
end

% to_plot = 1;
% fun_rrs_model_GIOP_global(w, k(1), k(2), k(3), k(4), k(5), k(6), to_plot);